async function renderdog() {
  const result = await fetch("https://dog.ceo/api/breeds/image/random");
  let data = await result.json();
  console.log(data.message);

  var doggo = `<div><img style="width: 300px; height: 300px;" src="${data.message}" alt="doggggg"></div>`;

  document.getElementById("doggo").innerHTML = doggo;
}

renderdog();

async function rendercat() {
  const result = await fetch("https://thatcopy.pw/catapi/rest/");
  let data = await result.json();
  console.log(data.url);

  var catto = `<div><img style="width: 300px; height: 300px;" src="${data.url}" alt="catttttttt"></div>`;

  document.getElementById("catto").innerHTML = catto;
}

rendercat();

async function renderchuck() {
  const result = await fetch("https://api.chucknorris.io/jokes/random");
  let data = await result.json();
  console.log(data.url);

  var chuck = `<div><p>${data.value}</p></div>`;

  document.getElementById("chuck").innerHTML = chuck;
}

renderchuck();

document.querySelector(".dog").addEventListener("click", renderdog);
document.querySelector(".cat").addEventListener("click", rendercat);
